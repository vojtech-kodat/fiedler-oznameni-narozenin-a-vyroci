#!/usr/bin/bash


compare_birthaday_date () {
  #funkce pro zjištění, jestli zaměstanec bude brzy slavit narozeniny
  #$compare

  date_in_days=$(date -d "$days_before days" +%F)

  IFS="-"
  read yy1 mm1 dd1 <<< $date_in_days
  read mm2 dd2 yy2 <<< $birth_date
  IFS=$OLDIFS

  #Pokud se datum za $days_before dní shoduje s daten narozenin, pošle narozeninovou zprávu
  if [ $mm1 == $mm2 ] && [ $dd1 == $dd2 ]
  then
    _msg="narozeniny! $((yy1 - yy2))"
    email_content=$email_txt
    sms_content=$sms_txt
    send_messages
  fi
}



manage_anniversaries () {
  #funkce pro zjištění, jestli zaměstanec bude v blízké době slavit výročí spolupráce

  IFS="-"
  read yy1 mm1 dd1 <<< $(date -d "$days_before days" +%F)
  read mm2 dd2 _yy2 <<< $hire_date
  IFS=$OLDIFS


  I=0

  #loop projde každý výroční rok. Když nastane schoda, pošle zprávu
  while true
  do
    I=$(($I + 1))
    years=$(($I * ${anniversary::-1}))
    yy2=$(($_yy2 + $years))
    #vypočítá výroční roky a uloží je do yy2

    if [ "$yy1" == "$yy2" ] && [ "$mm1" == "$mm2" ] && [ "$dd1" == "$dd2" ]
    then
      _msg="vyroci $((yy1 - _yy2)) let spoluprace!"
      email_content=$email_txt
      sms_content=$sms_txt
      send_messages
      break
    fi

    #když výroční rok překročí nynější rok, loop zkončí
    if [ $yy1 -le $yy2 ]
    then
      break
    fi
  done
}



send_messages () {
  #$_msg, $sms_content a $email_content je definováno tam odkud se tato fukce volá
  #$_msg obsahuje co se bude slavit a další info k tomu
  #$sms_content obsahuje zprávu, která bude poslána pod $header

  if [ "$header" == "NULL" ]
  then
    header="Zamestnanec $company $name $surname bude za $days_before dni slavit $_msg!"
  fi


  get=2
  got=-1

  cat seznam.csv|while read _ _ _phone email _
    do
      if [ "$got" == "-1" ]
      then
        got=$((1 + $got))
        continue
      fi
      phone=${_phone//\"}

      email_arr+="$email,"
      phone_arr+="$phone,"

      got=$((1 + $got))

      if [ "$got" == "$get" ]
      then
        bash send_email.sh <<< "$header;$email_content;$email_arr"
        bash send_sms.sh <<< "$header;$sms_content;$phone_arr"
        break
      fi
    done
    header="NULL"
}



birthday_today () {
  #funkce pro zjištění, jesli má někdo dnes narozeniny

  IFS="-"
  read yy1 mm1 dd1 <<< $(date +%F)
  read mm2 dd2 yy2 <<< $birth_date
  IFS=$OLDIFS

  if [ $mm1 == $mm2 ] && [ $dd1 == $dd2 ]
  then
    header="Zamestnanec $company $name $surname slavi dnes narozeniny!"
    email_content=$email_txt
    sms_content=$sms_txt
    send_messages
  fi
}



anniversary_today () {
  #funkce pro zjištění, jesli má někdo dnes narozeniny

  IFS="-"
  read yy1 mm1 dd1 <<< $(date +%F)
  read mm2 dd2 yy2 <<< $hire_date
  IFS=$OLDIFS

  if [ "$mm1" == "$mm2" ] && [ "$dd1" == "$dd2" ]
  then
    header="Zamestnanec $company $name $surname slavi dnes vyroci spoluprace!"
    email_content=$email_txt
    sms_content=$sms_txt
    send_messages
  fi
}



IFS=","
OLDIFS=$IFS

read -r line

# $name - jméno zaměstance
# $surname - příjmení zaměstnance
# $_phone - telefonní číslo zaněstnance s uvozovkami. Upraveno do $phone
# $email - email zaměstnance
# $title - pozice zaměstnance
# $company - název firmy
# $_birth_date - datum narození ve špatném formátu. Upraveno do $birth_date
# $_hire_date - datum zaměstnání ve špatném formátu. Upraveno do $hire_date
# $sms_txt - text, který se pošle v upozornění sms
# $email_txt - text, který se pošle v upozornění emailem
# $days_before - kolik dní předem bude skript dávat vědět o události
# $anniversary - kolikátá výročí se budou slavit

header="NULL"

while read name surname _phone email title company _birth_date _hire_date sms_txt email_txt days_before anniversary
do
  IFS="."
  read dd mm yy <<< $_birth_date
  birth_date="$mm-$dd-$yy"

  read dd mm yy <<< $_hire_date
  hire_date="$mm-$dd-$yy"
  IFS=$OLDIFS
  #převede data do požadovaného mm-dd-yy formátu

  compare_birthaday_date
  birthday_today
  manage_anniversaries
  anniversary_today
done
