#!/usr/bin/bash

_date="0"

#kdy se má zaslat oznámení
target_hour="15"

while true
do
  if [ $_date != $(date +%F) ]
  then
    while true
    do
      #program počká na určenou hodinu, ve kterou zkontroluje data a popřípadě pošle oznámení

      if [ "$target_hour" == $(date +%H) ]
      then
        echo "kontrola dat..."
        echo ""
        bash main.sh < "seznam.csv"
        _date=$(date +%F)
        break
      fi

    done
  fi

  sleep 1
done
