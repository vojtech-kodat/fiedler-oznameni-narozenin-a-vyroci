Skript každý den ve 12 hodin (nastavitelné) zkontroluje, jestli nějaký zaměstanec nebude mít narozeniny
nebo slavit výročí. Když ano, pošle oznámení na email a sms prvním dvoum zadaným v tabulce

Skript se nastartuje spuštěním run.sh

Email a sms odbdrzí pouze první dva zaměstnanci v tabulce

tabulka:
Jméno - Jméno zaměstnance bez háčků čárek
Příjmení - Příjmení zaměstnance bez háčků a čárek
telefon - telefonní číslo zaměstnance s předvolbou a v uvozovkách (např. "+420258873349") (nutno vyplnit pouze u prvních dvou
          v tabulce. U ostatních lze nechat pouze pomlčku)
email - email zaměstnance (nutno vyplnit pouze u prvních dvou v tabulce. U ostatních lze nechat pouze pomlčku)
pozice - pozice zaměstnance bez háčků a čárek
firma - firma, ve které pracuje
datum narození - data musí být zadávány ve formátu dd.mm.yy (např. 05.02.2005)
datum zaměstnání - data musí být zadávány ve formátu dd.mm.yy (např. 05.02.2005)
zprava sms - sms zpráva, která přijde s oznámením (není nutno vyplnit)
zprava email - zpráva, která přijde na email jako obsah (nepsat předmět). Nepsat diaktritiku ani čárky mezi slovy
upozornit dní předem - zadat číslo. O tolik dní bude pedem upozorněno na nadcházející událost
výročí - na jaká výročí upozorňovat (např. 5 znamená že bude upozorňovat na 5, 10, 15...)


!!!!nikde nepsat diaktritiku ani čárky mezi slovy. Políčko vždy vyplnit alespoň pomlčkou!!!!
