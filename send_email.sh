IFS=";"

passwd="agotrpqdjnhrqzqk" #heslo vytvořené pro přístup aplikací
sender_email="vojtakodat@gmail.com" #email musí být gmail

read header content people
echo "zasílání emailů..."

IFS=","

cat > email.txt
printf $content >> email.txt

for mail in $people
do
  curl --ssl-reqd \
    --url 'smtps://smtp.gmail.com:465' \
    --user "$sender_email:$passwd" \
    --mail-from "$sender_email" \
    --mail-rcpt "$mail" \
    -F "=<email.txt;" \
    -H "Subject: $header"
done
